﻿// requirement class
using Microsoft.AspNetCore.Authorization;
using Orgatron.Models;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

public class EstCreateurEvenementRequirement : IAuthorizationRequirement { }

// handler class
public class EstCreateurEvenementAuthorizationHandler : AuthorizationHandler<EstCreateurEvenementRequirement, Evenement>
{

    protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, EstCreateurEvenementRequirement requirement, Evenement evenement)
    {
        if (context.User == null || evenement == null)
        {
            return Task.CompletedTask;
        }

        if (evenement.Createur.Id == context.User.FindFirst(ClaimTypes.NameIdentifier).Value)
        {
            context.Succeed(requirement);
        }

        return Task.CompletedTask;
    }

}