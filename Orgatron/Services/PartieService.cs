﻿using Microsoft.AspNetCore.Identity;
using Orgatron.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.UI.Services;
using Orgatron.Helper;
using Orgatron.Tools;
using System.Text.Encodings.Web;
using System.Text;

namespace Orgatron.Services
{
    public class Participation
    {
        public IdentityUser User { get; set; }
        public Jeu Jeu { get; set; }
        public int NbParticipation { get; set; }
        public int NbParticipationJeu { get; set; }
    }



    public class PartieService
    {
        private Random _rnd = new Random();
        private readonly object managementLock = new object();
        private readonly IEmailSender _emailSender;

        public PartieService(IEmailSender emailSender)
        {
            _emailSender = emailSender;
        }

        /// <summary>
        /// Permet à un joueur d'accepter ou de refuser une invitation
        /// </summary>
        /// <param name="partie">Partie à arbitrer</param>
        /// <param name="context">contexte pour accéder à la BDD</param>
        /// <param name="reponse">EtatInvitation.Accepted pour oui ou EtatInvitation.Rejected pour non</param>
        /// <param name="message">Message qui indique pourquoi ça s'est mal passé ou si ça s'est bien passé</param>
        /// <returns>true si l'invitation a été prise en compte et false sinon</returns>
        public bool RepondreArbitrage(Partie partie, OrgatronContext context, EtatInvitation reponse, out string message)
        {
            lock (managementLock)
            {
                //Rafraichir l'objet invitation
                partie = context.Partie.Include(p => p.Evenement).Where(p => p.Id == partie.Id).FirstOrDefault();
                bool sortie = false;
                if (partie.Etat == EtatPartie.Cancelled)
                {
                    message = "La partie a été annulée par manque de participants ou d'arbitres";
                }
                else if (partie.Etat == EtatPartie.Started || partie.Etat == EtatPartie.Ended)
                {
                    message = "Vous avez été trop tardé à répondre, la partie a déjà démarré";
                }
                else if (partie.Etat == EtatPartie.Created)
                {
                    if (reponse == EtatInvitation.Rejected)
                    {
                        //On ajoute une participation refusée sur la partie pour comptabiliser les malus de refus
                        Participant nouveauParticipant = new Participant
                        {
                            Equipe = "refusArbitrage",
                            Partie = partie,
                            Points = 0,
                            User = partie.Arbitre,
                            Etat = EtatInvitation.Rejected
                        };
                        context.Participant.Add(nouveauParticipant);
                        context.SaveChanges();
                        InviterUnArbitreRemplacant(partie, context);
                        message = "Le refus a été pris en compte";
                    }
                    else
                    {
                        partie.EtatArbitre = reponse;
                        message = "L'inscription a été prise en compte";
                        context.SaveChanges();
                        LancerParties(partie.Evenement, context);
                    }
                    sortie = true;
                }
                else
                {
                    message = "Erreur inconnue";
                }
                return sortie;
            }
        }

        /// <summary>
        /// Permet à un joueur d'accepter ou de refuser une invitation
        /// </summary>
        /// <param name="invitation">Participant à la partie</param>
        /// <param name="context">contexte pour accéder à la BDD</param>
        /// <param name="reponse">EtatInvitation.Accepted pour oui ou EtatInvitation.Rejected pour non</param>
        /// <param name="message">Message qui indique pourquoi ça s'est mal passé ou si ça s'est bien passé</param>
        /// <returns>true si l'invitation a été prise en compte et false sinon</returns>
        public bool RepondreInvitation(Participant invitation, OrgatronContext context, EtatInvitation reponse, out string message)
        {
            lock (managementLock)
            {
                //Rafraichir l'objet invitation
                invitation = context.Participant.Include(p => p.Partie).Include(p => p.Partie.Evenement).Where(p => p.Id == invitation.Id).FirstOrDefault();
                bool sortie = false;
                if (invitation.Partie.Etat == EtatPartie.Cancelled)
                {
                    message = "La partie a été annulée par manque de participants ou d'arbitres";
                }
                else if (invitation.Partie.Etat == EtatPartie.Started || invitation.Partie.Etat == EtatPartie.Ended)
                {
                    message = "Vous avez été trop tardé à répondre, la partie a déjà démarré";
                }
                else if (invitation.Partie.Etat == EtatPartie.Created)
                {
                    if (reponse == EtatInvitation.Rejected)
                    {
                        InviterUnRemplacant(invitation, context);
                        invitation.Etat = reponse;
                        context.SaveChanges();
                        message = "Le refus a été pris en compte";
                    }
                    else
                    {
                        invitation.Etat = reponse;
                        context.SaveChanges();
                        message = "L'inscription a été prise en compte";
                        LancerParties(invitation.Partie.Evenement, context);
                    }
                    sortie = true;
                }
                else
                {
                    message = "Erreur inconnue";
                }
                return sortie;
            }
        }

        public void LancerParties(Evenement evenement, OrgatronContext context)
        {
            lock (managementLock)
            {
                GererPartiesEnAttente(context);

                /*
                // query the database using EF here.
                string query = context.Users.Where(u =>
                context.EvenementUtilisateur.Where(eu => eu.Evenement == evenement && eu.User == u).Any()
                && !context.Participant.Where(p => p.User.Id == u.Id && (p.Partie.Etat == EtatPartie.Created || p.Partie.Etat == EtatPartie.Started) && p.Etat != EtatInvitation.Rejected).Any()
                && !context.Partie.Where(p => p.Arbitre == u && (p.Etat == EtatPartie.Created || p.Etat == EtatPartie.Started) && p.EtatArbitre != EtatInvitation.Rejected).Any()
                ).ToSql();
                */

                //Lister les jeux et joueurs disponibles
                List<IdentityUser> joueursDisponibles = GetJoueursDisponibles(evenement.Id, context);

                if (joueursDisponibles.Count() < evenement.NbPersonnesEnAttente || DateTime.Now > evenement.EndAt)
                {
                    return;
                }

                List<Participation> partiesjouees = context.Participant.Where(p => p.Partie.Evenement == evenement && p.Etat != EtatInvitation.Rejected && p.Partie.Etat != EtatPartie.Cancelled).GroupBy(e => new { e.User, e.Partie.Jeu }).Select(e => new Participation() { User = e.Key.User, Jeu = e.Key.Jeu, NbParticipationJeu = e.Count() }).ToList();

                
                List<Participation> partiesjoueur = null;
                foreach (var joueur in joueursDisponibles)
                {
                    partiesjoueur = partiesjouees.Where(p => p.User == joueur).ToList();
                    int NbPartiesTot = partiesjoueur.Sum(p => p.NbParticipationJeu);
                    foreach (var partiejouee in partiesjouees.Where(p => p.User == joueur))
                    {
                        partiejouee.NbParticipation = NbPartiesTot;
                    }
                }

                List<Participation> partiesarbitrees = context.Partie.Where(p => p.Evenement == evenement && p.Etat != EtatPartie.Cancelled).GroupBy(e => new { e.Arbitre }).Select(e => new Participation() { User = e.Key.Arbitre, Jeu = null, NbParticipation = e.Count() }).ToList();

                //Selectionner les jeux disponibles
                List<Jeu> jeuxDisponibles = context.Jeu.Where(j => j.EstActif == true && (j.NbEquipesMax * j.NbParticipantsParEquipesMax + 1) <= joueursDisponibles.Count() && !context.Partie.Where(p => p.Evenement == evenement && p.Jeu.Id == j.Id && (p.Etat == EtatPartie.Created || p.Etat == EtatPartie.Started)).Any())
                    //Les trier par nombre de parties lancées descendantes (on prend en compte mêmes les parties annulées que les gens n'ont pas voulues)
                    .GroupJoin(
                        context.Partie.Where(p => p.Evenement == evenement).GroupBy(e => e.Jeu).Select(n => new { jeu = n.Key, Nombre = n.Count() }),
                        jeu => jeu,
                        partie => partie.jeu,
                        (jeu, partie) => new { jeu, partie }
                    ).SelectMany(
                        x => x.partie.DefaultIfEmpty(),
                        (x, y) => new { x.jeu, Nombre = y == null ? 0 : y.Nombre })
                        .ToList().Shuffle()
                        .OrderBy(j => j.Nombre).Select(j => j.jeu)
                    .ToList();


                while (jeuxDisponibles.Count() > 0)
                {
                    //Selectionner un jeu aléatoirement dans la liste
                    Jeu jeu = jeuxDisponibles[0];
                    jeuxDisponibles.Remove(jeu);

                    IdentityUser arbitreSelectionne = SelectionnerArbitre(joueursDisponibles, partiesarbitrees);
                    List<IdentityUser> joueursSelectionnes = SelectionnerJoueurs(jeu, partiesjouees, joueursDisponibles, jeu.NbParticipantsParEquipesMax * jeu.NbEquipesMax);

                    //On crée une partie
                    Partie nouvellePartie = new Partie
                    {
                        Etat = EtatPartie.Created,
                        Evenement = evenement,
                        Jeu = jeu,
                        Arbitre = arbitreSelectionne,
                        EtatArbitre = EtatInvitation.Accepted,
                        DateCreation = DateTime.Now
                    };


                    //Sélectionner un arbitre aléatoirement
                    context.Partie.Add(nouvellePartie);

                    //Affecter des joueurs aléatoirement dans la liste des joueurs dispo
                    for (int i = 0; i < jeu.NbEquipesMax; i++)
                    {
                        for (int j = 0; j < jeu.NbParticipantsParEquipesMax; j++)
                        {
                            Participant nouveauParticipant = new Participant
                            {
                                Equipe = i.ToString(),
                                Partie = nouvellePartie,
                                Points = 0,
                                User = PickElementAndRemoveItFromList(joueursSelectionnes),
                                Etat = EtatInvitation.Accepted
                            };
                            context.Participant.Add(nouveauParticipant);
                        }
                    }
                    //on envoie un message aux joueurs dans un autre process
                    _ = EnvoyerMessageAuxJoueursAsync(nouvellePartie);

                    //On supprime les jeux de la liste pour lesquels nous n'avons plus assez de joueurs
                    jeuxDisponibles = jeuxDisponibles.Where(j => (j.NbEquipesMax * j.NbParticipantsParEquipesMax + 1) <= joueursDisponibles.Count()).ToList();

                }
                context.SaveChanges();
            }
        }

        private IdentityUser SelectionnerArbitre(List<IdentityUser> joueursDisponibles, List<Participation> partiesarbitrees)
        {
            List<IdentityUser> joueurSelectionne =
                joueursDisponibles.GroupJoin(
                partiesarbitrees,
                joueursDispo => joueursDispo,
                particiations => particiations.User,
                (joueursDispo, particiations) => new { user = joueursDispo, particiations }
                 ).SelectMany(
                x => x.particiations.DefaultIfEmpty(),
                (x, y) => new { x.user, NbParticipation = y == null ? 0 : y.NbParticipation })
                .ToList().Shuffle()
                .OrderBy(p => p.NbParticipation).Select(e => e.user).ToList();


            joueursDisponibles.Remove(joueurSelectionne[0]);
            return joueurSelectionne[0];
        }

        private void InviterUnArbitreRemplacant(Partie partie, OrgatronContext context)
        {
            List<IdentityUser> joueursDisponibles = GetJoueursDisponibles(partie.Evenement.Id, context);

            //On retire les joueurs qui ont déjà refusé cette partie
            List<IdentityUser> joueursDejaSelectionnes = context.Participant.Include(p => p.User).Where(p => p.Partie == partie && p.Etat == EtatInvitation.Rejected).Select(p => p.User).ToList();
            foreach (IdentityUser joueur in joueursDejaSelectionnes)
            {
                joueursDisponibles.Remove(joueur);
            }

            if (joueursDisponibles.Count() > 0)
            {
                partie.Arbitre = PickElementAndRemoveItFromList(joueursDisponibles);
                context.SaveChanges();
                //Envoyer un message sans attendre la réponse
                _ = EnvoyerMessageAuxJoueursAsync(partie, new List<String> { partie.Arbitre.Email });
            }
            else
            {
                partie.EtatArbitre = EtatInvitation.Rejected;
                context.SaveChanges();
                LancerParties(partie.Evenement, context);
            }
        }

        private List<IdentityUser> SelectionnerJoueurs(Jeu jeu, List<Participation> partiesjouees, List<IdentityUser> joueursDisponibles, int nbParticipants)
        {
            List<IdentityUser> joueursSelectionnes = joueursDisponibles.GroupJoin(
                partiesjouees.Where(p => p.Jeu.Id == jeu.Id),
                joueursDispo => joueursDispo,
                particiations => particiations.User,
                (joueursDispo, particiations) => new { user = joueursDispo, particiations }
                 ).SelectMany(
                x => x.particiations.DefaultIfEmpty(),
                (x, y) => new { x.user, NbParticipation = y == null ? 0 : y.NbParticipation, NbParticipationJeu = y == null ? 0 : y.NbParticipationJeu })
                .ToList().Shuffle()
                .OrderBy(p => p.NbParticipationJeu).ThenBy(p => p.NbParticipation).Select(e => e.user).Take(nbParticipants).ToList();

            foreach (IdentityUser user in joueursSelectionnes)
            {
                joueursDisponibles.Remove(user);
            }

            return joueursSelectionnes;


        }

        private List<IdentityUser> GetJoueursDisponibles(int idEvenement, OrgatronContext context)
        {
            return context.Users.Where(u =>
                       context.EvenementUtilisateur.Where(eu => eu.Evenement.Id == idEvenement && eu.User == u && eu.EstEnPause == false).Any()
                       && !context.Participant.Where(p => p.User.Id == u.Id && (p.Partie.Etat == EtatPartie.Created || p.Partie.Etat == EtatPartie.Started) && p.Etat != EtatInvitation.Rejected).Any()
                       && !context.Partie.Where(p => p.Arbitre == u && (p.Etat == EtatPartie.Created || p.Etat == EtatPartie.Started) && p.EtatArbitre != EtatInvitation.Rejected).Any()
                   ).ToList();
        }

        private void InviterUnRemplacant(Participant invitation, OrgatronContext context)
        {
            List<IdentityUser> joueursDisponibles = GetJoueursDisponibles(invitation.Partie.Evenement.Id, context);

            //On retire les joueurs qui ont déjà refusé cette partie
            List<IdentityUser> joueursDejaSelectionnes = context.Participant.Include(p => p.User).Where(p => p.Partie == invitation.Partie && p.Etat == EtatInvitation.Rejected).Select(p => p.User).ToList();
            foreach (IdentityUser joueur in joueursDejaSelectionnes)
            {
                joueursDisponibles.Remove(joueur);
            }

            if (joueursDisponibles.Count() > 0)
            {
                Participant nouveauParticipant = new Participant
                {
                    Equipe = invitation.Equipe,
                    Partie = invitation.Partie,
                    Points = 0,
                    User = PickElementAndRemoveItFromList(joueursDisponibles),
                    Etat = EtatInvitation.Accepted
                };
                context.Participant.Add(nouveauParticipant);
                context.SaveChanges();
                //Envoyer un message sans attendre la réponse
                _ = EnvoyerMessageAuxJoueursAsync(invitation.Partie, new List<String> { nouveauParticipant.User.Email });
            }
            else
            {
                invitation.Etat = EtatInvitation.Rejected;
                context.SaveChanges();
                LancerParties(invitation.Partie.Evenement, context);
            }
        }

        private void GererPartiesEnAttente(OrgatronContext context)
        {
            List<Partie> PartiesEnAttente = context.Partie.Include(i => i.Jeu).Where(p => p.Etat == EtatPartie.Created).ToList();

            foreach (var partie in PartiesEnAttente)
            {
                int nbEquipesEncoreConstructible = context.Participant.Where(p => p.Partie == partie && p.Etat != EtatInvitation.Rejected)
               .GroupBy(p => p.Equipe)
               .Select(g => new { equipe = g.Key, count = g.Count() }).Where(g => g.count >= partie.Jeu.NbParticipantsParEquipesMin).Count();

                //Une partie n'ayant pas assez de joueurs
                if (nbEquipesEncoreConstructible < partie.Jeu.NbEquipesMin || partie.EtatArbitre == EtatInvitation.Rejected)
                {
                    partie.Etat = EtatPartie.Cancelled;
                    //On n'attends pas que les mails aient été envoyés pour continuer l'exécution
                    _ = EnvoyerMessageAuxJoueursAsync(partie);
                }
            }
            context.SaveChanges();
        }

        private T PickElementAndRemoveItFromList<T>(IList<T> list)
        {
            T element = list[_rnd.Next(list.Count)];
            list.Remove(element);
            return element;
        }

        private async System.Threading.Tasks.Task EnvoyerMessageAuxJoueursAsync(Partie partie, List<String> Emails = null)
        {
            //Définition de l'objet du mail en fonction de l'état de la partie
            string objetMail = "";
            if (partie.Etat == EtatPartie.Created)
            {
                objetMail = String.Format("Début jeu - {0}", partie.Jeu.Nom);
            }
            else if (partie.Etat == EtatPartie.Cancelled)
            {
                objetMail = String.Format("Annulé - {0}", partie.Jeu.Nom);
            }
            else
            {
                //On n'envoie pas de mail en dehors de ces états de partie
                return;
            }

            string callbackUrl = String.Format("https://jeu.amoki.fr/Evenements/Home?id={0}", partie.Evenement.Id);

            StringBuilder contenu = new StringBuilder();
            contenu.AppendLine("<p>");
            contenu.AppendLine(String.Format("Jeu : {0}", partie.Jeu.Nom));
            contenu.AppendLine("</p>");

            contenu.Append("<p>");
            contenu.AppendLine(String.Format("Arbitre : {0}", partie.Arbitre.UserName));
            contenu.AppendLine("</p>");

            contenu.Append("<p>");
            contenu.AppendLine("Joueurs :");
            contenu.AppendLine("</p>");

            contenu.AppendLine("<ul>");
            foreach (var item in partie.Participants)
            {
                contenu.AppendLine("<li>");
                contenu.AppendLine(String.Format("{0}(Equipe {1})", item.User.UserName, item.Equipe));
                contenu.AppendLine("</li>");
            }
            contenu.AppendLine("</ul>");
            contenu.AppendLine("La liste des joueurs est suceptible de changer si certains refusent. La liste à jour est disponible sur le site :");
            contenu.AppendLine(String.Format("<a href={0}>Aller sur le site</a>", HtmlEncoder.Default.Encode(callbackUrl)));

            if (Emails == null)
            {
                Emails = partie.Participants.Select(p => p.User.Email).ToList();
                Emails.Add(partie.Arbitre.Email);
            }

            foreach (string email in Emails)
            {
                await _emailSender.SendEmailAsync(
                   email,
                   objetMail,
                   contenu.ToString());
            }
        }

    }
}
