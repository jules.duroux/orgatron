﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Orgatron.Models
{

    public enum EtatPartie
    {
        [Description("Inscriptions en cours")]
        Created,
        [Description("Démarrée")]
        Started,
        [Description("Annulée")]
        Cancelled,
        [Description("Terminée")]
        Ended
    }

    public enum EtatChrono
    {
        Created,
        Started,
        Paused,
    }

    public class Partie
    {
        public int Id { get; set; }
        public Jeu Jeu { get; set; }
        public Evenement Evenement { get; set; }
        public IdentityUser Arbitre { get; set; }
        public EtatPartie Etat { get; set; }
        public DateTime DateCreation { get; set; }
        public DateTime DateDebut { get; set; }
        public DateTime DateFin { get; set; }
        public EtatInvitation EtatArbitre { get; set; }
        [DataType(DataType.MultilineText)]
        public string NotesArbitre { get; set; }
        public DateTime DateDebutChrono { get; set; }
        public DateTime DateDebutPause { get; set; }
        public double NbSecondesPauseChrono { get; set; }
        public EtatChrono EtatChrono { get; set; }

        public virtual IList<Participant> Participants { get; set; }
    }
}