﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Orgatron.Models
{
    public class Evenement
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public IdentityUser Createur { get; set; }
        public DateTime StartedAt { get; set; }
        public DateTime EndAt { get; set; }
        public int NbPersonnesEnAttente { get; set; }

        public virtual ICollection<EvenementUtilisateur> EvenementsUtilisateurs { get; set; }
        public virtual ICollection<Partie> Parties { get; set; }
    }
}