using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Identity.UI.Services;
using WebPWrecover.Services;
using Orgatron.Services;
using Orgatron.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Identity;
using Orgatron.Seeds;
using Westwind.AspNetCore.Markdown;
using Markdig.Extensions.AutoIdentifiers;
using Markdig;
using Orgatron.Batchs;

namespace Orgatron
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.ConfigureApplicationCookie(options =>
            {
                // Cookie settings
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(300);
                options.Cookie.IsEssential = true;

                options.LoginPath = "/Identity/Account/Login";
                options.AccessDeniedPath = "/Identity/Account/AccessDenied";
                options.SlidingExpiration = true;
            });

            services.Configure<IdentityOptions>(options =>
            {
                // Password settings.
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 6;
                options.Password.RequiredUniqueChars = 3;

                options.SignIn.RequireConfirmedEmail = true;

                // Lockout settings.
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.AllowedForNewUsers = true;

                // User settings.
                options.User.AllowedUserNameCharacters =
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+ ���������������";
                options.User.RequireUniqueEmail = true;
            });


            //Dur�e de vie token inscription
            services.Configure<DataProtectionTokenProviderOptions>(options =>
            {
                options.TokenLifespan = TimeSpan.FromDays(2);
            });

            services.AddMvc(config =>
            {
                var policy = new AuthorizationPolicyBuilder()
                                 .RequireAuthenticatedUser()
                                 .Build();
                config.Filters.Add(new AuthorizeFilter(policy));
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddDbContext<OrgatronContext>(options =>
                    options.UseSqlServer(
                        Configuration.GetConnectionString("OrgatronContextConnection")));

            services.AddSingleton<IEmailSender, EmailSender>();
            services.Configure<AuthMessageSenderOptions>(Configuration);
            services.AddSingleton<PartieService>();

            // authorization policies & requirements
            services.AddAuthorization(options =>
            {
                options.AddPolicy("UserInEvenement", policy =>
                    policy.Requirements.Add(new UserInEvenementRequirement()));
                options.AddPolicy("EstCreateurEvenement", policy =>
                    policy.Requirements.Add(new EstCreateurEvenementRequirement()));
                options.AddPolicy("EstArbitrePartie", policy =>
                    policy.Requirements.Add(new EstArbitrePartieRequirement()));    
                options.AddPolicy("AdminOnly", policy => policy.RequireClaim("Admin"));
            });

            // Authorization handlers.
            services.AddScoped<IAuthorizationHandler, UserInEvenementAuthorizationHandler>();
            services.AddScoped<IAuthorizationHandler, EstCreateurEvenementAuthorizationHandler>();
            services.AddScoped<IAuthorizationHandler, EstArbitrePartieAuthorizationHandler>();

            services.AddMarkdown();

            //Batch pour clore les parties au bout de 5 minutes si les jouers n'ont pas accept�s
            services.AddHostedService<Cleanup>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                //app.UseHsts();
            }

            app.UseMarkdown();
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseCookiePolicy();
            app.UseMvc();

            bool hasDoneMigrate = MigrateDatabase.ApplyMigrations(app.ApplicationServices).GetAwaiter().GetResult();
            if (hasDoneMigrate)
            {
                UsersData.SeedAdmins(app.ApplicationServices, new List<Tuple<string, string>> { new Tuple<string, string>(Configuration["AdminEmail"], Configuration["AdminPassword"]) }).Wait();
            }
        }
    }
}
