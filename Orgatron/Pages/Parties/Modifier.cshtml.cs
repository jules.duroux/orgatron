﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Orgatron.Models;

namespace Orgatron.Pages.Parties
{
    public class ModifierModel : PageModel
    {
        private readonly OrgatronContext _context;
        public readonly IAuthorizationService _authorizationService;

        public ModifierModel(OrgatronContext context, IAuthorizationService authorizationService)
        {
            _context = context;
            _authorizationService = authorizationService;
        }

        public Partie Partie { get; set; }

        [BindProperty]
        public InputJoueurModel InputJoueur { get; set; }
        public class InputJoueurModel
        {
            public int ParticipantId { get; set; }
            public string UserId { get; set; }
            public string Equipe { get; set; }
            public int Points { get; set; }
            public string EtatParticipant { get; set; }
            public int PartieId { get; set; }
        }

        [BindProperty]
        public InputPartieModel InputPartie { get; set; }
        public class InputPartieModel
        {
            public int PartieId { get; set; }
            public string UserId { get; set; }
            public string JeuId { get; set; }
            public DateTime DateDebut { get; set; }
            public DateTime DateFin { get; set; }
            public string EtatPartie { get; set; }
        }

        public IEnumerable<SelectListItem> ListeJoueurs { get; set; }
        public IEnumerable<SelectListItem> ListeJeux { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Partie = await _context.Partie.Include(p => p.Arbitre).Include(p => p.Jeu).Include(p => p.Evenement).Include(p => p.Evenement.Createur).Include(p => p.Participants).ThenInclude(participant => participant.User).FirstOrDefaultAsync(m => m.Id == id);

            var isAuthorized = await _authorizationService.AuthorizeAsync(User, "AdminOnly");
            if (!isAuthorized.Succeeded)
            {
                if (User.Identity.IsAuthenticated)
                {
                    return new ForbidResult();
                }
                else
                {
                    return new ChallengeResult();
                }
            }


            ListeJoueurs = _context.EvenementUtilisateur.Include(p => p.User).Where(p => p.Evenement == Partie.Evenement).Select(i => new SelectListItem()
            {
                Text = i.User.UserName,
                Value = i.User.Id
            });

            ListeJeux = _context.Jeu.Where(j => j.EstActif == true).Select(i => new SelectListItem()
            {
                Text = i.Nom,
                Value = i.Id.ToString()
            });

            if (Partie == null)
            {
                return NotFound();
            }

            return Page();
        }


        public async Task<IActionResult> OnPostAddPlayerAsync()
        {
            Partie = await _context.Partie.Include(p => p.Evenement).Include(p => p.Evenement.Createur).FirstOrDefaultAsync(m => m.Id == InputJoueur.PartieId);

            if (Partie == null)
            {
                return NotFound();
            }

            var isAuthorized = await _authorizationService.AuthorizeAsync(User, "AdminOnly");
            if (!isAuthorized.Succeeded)
            {
                if (User.Identity.IsAuthenticated)
                {
                    return new ForbidResult();
                }
                else
                {
                    return new ChallengeResult();
                }
            }

            Participant nouveauParticipant = new Participant();
            IdentityUser nouveauJoueur = _context.Users.Find(InputJoueur.UserId);
            nouveauParticipant.User = nouveauJoueur;
            nouveauParticipant.Points = InputJoueur.Points;
            nouveauParticipant.Equipe = InputJoueur.Equipe;
            nouveauParticipant.Etat = (EtatInvitation)Int32.Parse(InputJoueur.EtatParticipant);
            nouveauParticipant.Partie = Partie;
            nouveauParticipant.Notes = null;

            _context.Participant.Add(nouveauParticipant);

            _context.SaveChanges();

            return RedirectToPage("/Parties/Modifier", new { id = Partie.Id });

        }

        public async Task<IActionResult> OnPostEditPlayerAsync()
        {
            Participant participantAModifier = _context.Participant.Include(p => p.Partie).Where(p => p.Id == InputJoueur.ParticipantId).FirstOrDefault();
            if (participantAModifier == null)
            {
                return NotFound();
            }

            Partie = await _context.Partie.Include(p => p.Evenement).Include(p => p.Evenement.Createur).FirstOrDefaultAsync(m => m.Id == participantAModifier.Partie.Id);

            if (Partie == null)
            {
                return NotFound();
            }

            var isAuthorized = await _authorizationService.AuthorizeAsync(User, "AdminOnly");
            if (!isAuthorized.Succeeded)
            {
                if (User.Identity.IsAuthenticated)
                {
                    return new ForbidResult();
                }
                else
                {
                    return new ChallengeResult();
                }
            }

            IdentityUser nouveauJoueur = _context.Users.Find(InputJoueur.UserId);
            participantAModifier.User = nouveauJoueur;
            participantAModifier.Points = InputJoueur.Points;
            participantAModifier.Equipe = InputJoueur.Equipe;
            participantAModifier.Etat = (EtatInvitation)Int32.Parse(InputJoueur.EtatParticipant);

            _context.SaveChanges();

            return RedirectToPage("/Parties/Modifier", new { id = Partie.Id });

        }

        public async Task<IActionResult> OnPostEditPartieAsync()
        {
            Partie = await _context.Partie.Include(p => p.Arbitre).Include(p => p.Jeu).Include(p => p.Evenement).Include(p => p.Evenement.Createur).Include(p => p.Participants).ThenInclude(participant => participant.User).FirstOrDefaultAsync(m => m.Id == InputPartie.PartieId);

            if (Partie == null)
            {
                return NotFound();
            }

            var isAuthorized = await _authorizationService.AuthorizeAsync(User, "AdminOnly");
            if (!isAuthorized.Succeeded)
            {
                if (User.Identity.IsAuthenticated)
                {
                    return new ForbidResult();
                }
                else
                {
                    return new ChallengeResult();
                }
            }

            ListeJoueurs = _context.EvenementUtilisateur.Include(p => p.User).Where(p => p.Evenement == Partie.Evenement).Select(i => new SelectListItem()
            {
                Text = i.User.UserName,
                Value = i.User.Id
            });

            ListeJeux = _context.Jeu.Where(j => j.EstActif == true).Select(i => new SelectListItem()
            {
                Text = i.Nom,
                Value = i.Id.ToString()
            });

            Partie.Arbitre = _context.Users.Find(InputPartie.UserId);
            Partie.EtatArbitre = EtatInvitation.Accepted;
            Partie.Etat = (EtatPartie)Int32.Parse(InputPartie.EtatPartie);
            Partie.Jeu = _context.Jeu.Find(Int32.Parse(InputPartie.JeuId));

            _context.SaveChanges();

            return Page();
        }

        public async Task<IActionResult> OnPostDeletePlayerAsync()
        {
            Participant participantAModifier = _context.Participant.Include(p => p.Partie).Where(p => p.Id == InputJoueur.ParticipantId).FirstOrDefault();
            Partie = participantAModifier.Partie;
            if (participantAModifier == null)
            {
                return NotFound();
            }

            var isAuthorized = await _authorizationService.AuthorizeAsync(User, "AdminOnly");
            if (!isAuthorized.Succeeded)
            {
                if (User.Identity.IsAuthenticated)
                {
                    return new ForbidResult();
                }
                else
                {
                    return new ChallengeResult();
                }
            }

            _context.Participant.Remove(participantAModifier);

            _context.SaveChanges();

            return RedirectToPage("/Parties/Modifier", new { id = Partie.Id });

        }

    }
}
