﻿using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Orgatron.Models;
using Orgatron.Services;

namespace Orgatron.Pages.Parties
{
    public class RepondreArbitrageModel : PageModel
    {
        private readonly OrgatronContext _context;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly PartieService _partieService;
        private string _message;

        public RepondreArbitrageModel(OrgatronContext context, UserManager<IdentityUser> userManager, PartieService partieService)
        {
            _context = context;
            _userManager = userManager;
            _partieService = partieService;
        }

        public string Message {
            get { return _message; }
        }

        public string IdEvenement { get; set; }


        public IActionResult OnGet(int? idPartie, string reponse)
        {
            if (idPartie == null || reponse == null)
            {
                return NotFound();
            }

            Partie invitation = _context.Partie.Include(p => p.Arbitre).Include(p => p.Evenement).Where(p => p.Id == idPartie).FirstOrDefault();

            bool autorisation = invitation != null && invitation.Arbitre.Id == User.FindFirst(ClaimTypes.NameIdentifier).Value;
            if (!autorisation)
            {
                if (User.Identity.IsAuthenticated)
                {
                    return new ForbidResult();
                }
                else
                {
                    return new ChallengeResult();
                }
            }
            bool success = false;

            if (reponse == "accept")
            {
                success = _partieService.RepondreArbitrage(invitation, _context, EtatInvitation.Accepted, out _message);
                if (success)
                {
                    return RedirectToPage("/Parties/Details", new { invitation.Id });
                }
            }
            else if (reponse == "deny")
            {
                success = _partieService.RepondreArbitrage(invitation, _context, EtatInvitation.Rejected, out _message);
                if (success)
                {
                    return RedirectToPage("/Evenements/Home", new { invitation.Evenement.Id });
                }
            }
            
           

            IdEvenement = invitation.Evenement.Id.ToString();

            return Page();
        }
    }
}
