﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Orgatron.Models;

namespace Orgatron.Pages.Parties
{
    public class DetailsModel : PageModel
    {
        private readonly OrgatronContext _context;
        public readonly IAuthorizationService _authorizationService;

        public DetailsModel(OrgatronContext context, IAuthorizationService authorizationService)
        {
            _context = context;
            _authorizationService = authorizationService;
        }

        [BindProperty]
        public Partie Partie { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Partie = await _context.Partie.Include(p => p.Arbitre).Include(p => p.Jeu).Include(p => p.Evenement).Include(p => p.Participants).ThenInclude(participant => participant.User).FirstOrDefaultAsync(m => m.Id == id);
            Partie.Participants = Partie.Participants.Where(p => p.Etat != EtatInvitation.Rejected).ToList();

            if (Partie == null)
            {
                return NotFound();
            }

            return Page();
        }

        public async Task<IActionResult> OnPostStartChronoAsync(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }

            Partie = await _context.Partie.Include(p => p.Arbitre).Include(p => p.Jeu).Include(p => p.Evenement).Include(p => p.Participants).ThenInclude(participant => participant.User).FirstOrDefaultAsync(m => m.Id == id);
            Partie.Participants = Partie.Participants.Where(p => p.Etat != EtatInvitation.Rejected).ToList();

            if (Partie == null)
            {
                return NotFound();
            }
            var isAuthorized = await _authorizationService.AuthorizeAsync(User, Partie, "EstArbitrePartie");
            if (!isAuthorized.Succeeded)
            {
                if (User.Identity.IsAuthenticated)
                {
                    return new ForbidResult();
                }
                else
                {
                    return new ChallengeResult();
                }
            }
            
            if (Partie.DateDebutChrono == DateTime.MinValue)
            {
                Partie.DateDebutChrono = DateTime.Now;
            }
            if (Partie.EtatChrono == EtatChrono.Paused && Partie.DateDebutPause != DateTime.MinValue)
            {
                Partie.NbSecondesPauseChrono += (DateTime.Now - Partie.DateDebutPause).TotalSeconds;
            }
            Partie.EtatChrono = EtatChrono.Started;

            _context.SaveChanges();

            return Page();
        }

        public async Task<IActionResult> OnPostPauseChronoAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Partie = await _context.Partie.Include(p => p.Arbitre).Include(p => p.Jeu).Include(p => p.Evenement).Include(p => p.Participants).ThenInclude(participant => participant.User).FirstOrDefaultAsync(m => m.Id == id);
            Partie.Participants = Partie.Participants.Where(p => p.Etat != EtatInvitation.Rejected).ToList();

            if (Partie == null)
            {
                return NotFound();
            }

            var isAuthorized = await _authorizationService.AuthorizeAsync(User, Partie, "EstArbitrePartie");
            if (!isAuthorized.Succeeded)
            {
                if (User.Identity.IsAuthenticated)
                {
                    return new ForbidResult();
                }
                else
                {
                    return new ChallengeResult();
                }
            }

            Partie.DateDebutPause = DateTime.Now;
            Partie.EtatChrono = EtatChrono.Paused;

            _context.SaveChanges();

            return Page();
        }

        public async Task<IActionResult> OnPostDemarrerPartieAsync(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }

            Partie = await _context.Partie.Include(p => p.Arbitre).Include(p => p.Jeu).Include(p => p.Evenement).Include(p => p.Participants).ThenInclude(participant => participant.User).FirstOrDefaultAsync(m => m.Id == id);

            if (Partie == null || Partie.Etat != EtatPartie.Created)
            {
                return NotFound();
            }
            var isAuthorized = await _authorizationService.AuthorizeAsync(User, Partie, "EstArbitrePartie");
            if (!isAuthorized.Succeeded)
            {
                if (User.Identity.IsAuthenticated)
                {
                    return new ForbidResult();
                }
                else
                {
                    return new ChallengeResult();
                }
            }

            Partie.Etat = EtatPartie.Started;
            Partie.DateDebut = DateTime.Now;

            _context.SaveChanges();

            //On modifie l'affichage
            Partie.Participants = Partie.Participants.Where(p => p.Etat != EtatInvitation.Rejected).ToList();

            return Page();
        }

        public async Task<IActionResult> OnPostRebootChronoAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Partie = await _context.Partie.Include(p => p.Arbitre).Include(p => p.Jeu).Include(p => p.Evenement).Include(p => p.Participants).ThenInclude(participant => participant.User).FirstOrDefaultAsync(m => m.Id == id);
            Partie.Participants = Partie.Participants.Where(p => p.Etat != EtatInvitation.Rejected).ToList();

            if (Partie == null)
            {
                return NotFound();
            }

            var isAuthorized = await _authorizationService.AuthorizeAsync(User, Partie, "EstArbitrePartie");
            if (!isAuthorized.Succeeded)
            {
                if (User.Identity.IsAuthenticated)
                {
                    return new ForbidResult();
                }
                else
                {
                    return new ChallengeResult();
                }
            }

            Partie.DateDebutChrono = DateTime.Now;
            Partie.DateDebutPause = DateTime.MinValue;
            Partie.NbSecondesPauseChrono = 0;
            Partie.EtatChrono = EtatChrono.Started;

            _context.SaveChanges();

            return Page();
        }

    }
}
