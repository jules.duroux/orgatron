﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Orgatron.Models;

namespace Orgatron.Pages.Jeux
{
    public class IndexModel : PageModel
    {
        private readonly Orgatron.Models.OrgatronContext _context;
        private IAuthorizationService _authorizationService;

        public IndexModel(Orgatron.Models.OrgatronContext context, IAuthorizationService authorizationService)
        {
            _context = context;
            _authorizationService = authorizationService;
        }

        public IList<Jeu> Jeu { get;set; }

        public async Task OnGetAsync()
        {

            var isAuthorized = await _authorizationService.AuthorizeAsync(User, "AdminOnly");
            if (isAuthorized.Succeeded)
            {
                Jeu = await _context.Jeu.ToListAsync();
            }
            else
            {
                Jeu = await _context.Jeu.Where(j => j.EstActif == true).ToListAsync();
            }
            
        }
    }
}
