﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Orgatron.Models;

namespace Orgatron.Pages.Jeux
{
    public class DetailsModel : PageModel
    {
        private readonly Orgatron.Models.OrgatronContext _context;

        public DetailsModel(Orgatron.Models.OrgatronContext context)
        {
            _context = context;
        }

        public Jeu Jeu { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Jeu = await _context.Jeu.FirstOrDefaultAsync(m => m.Id == id);

            if (Jeu == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
