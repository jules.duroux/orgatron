﻿using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Orgatron.Models;
using Orgatron.Services;

namespace Orgatron.Pages.Participants
{
    public class AnswerInvitationModel : PageModel
    {
        private readonly OrgatronContext _context;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly PartieService _partieService;
        private string _message;

        public AnswerInvitationModel(OrgatronContext context, UserManager<IdentityUser> userManager, PartieService partieService)
        {
            _context = context;
            _userManager = userManager;
            _partieService = partieService;
        }

        public string Message {
            get { return _message; }
        }

        public string IdEvenement { get; set; }


        public IActionResult OnGet(int? idInvitation, string reponse)
        {
            if (idInvitation == null || reponse == null)
            {
                return NotFound();
            }

            Participant invitation = _context.Participant.Include(p => p.User).Include(p => p.Partie.Evenement).Where(p => p.Id == idInvitation).FirstOrDefault();

            bool autorisation = invitation != null && invitation.User.Id == User.FindFirst(ClaimTypes.NameIdentifier).Value;
            if (!autorisation)
            {
                if (User.Identity.IsAuthenticated)
                {
                    return new ForbidResult();
                }
                else
                {
                    return new ChallengeResult();
                }
            }

            bool success = false;

            if (reponse == "accept")
            {
                success = _partieService.RepondreInvitation(invitation, _context, EtatInvitation.Accepted, out _message);
                if (success)
                {
                    return RedirectToPage("/Parties/Details", new { invitation.Partie.Id });
                }
            }
            else if (reponse == "deny")
            {
                success = _partieService.RepondreInvitation(invitation, _context, EtatInvitation.Rejected, out _message);
                if (success)
                {
                    return RedirectToPage("/Evenements/Home", new { invitation.Partie.Evenement.Id });
                }
            }

            IdEvenement = invitation.Partie.Evenement.Id.ToString();

            return Page();
        }
    }
}
