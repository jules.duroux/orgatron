﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Orgatron.Models;
using Orgatron.Services;

namespace Orgatron.Pages.Parties
{
    public class RepondreInvitationAutreJoueurModel : PageModel
    {
        private readonly OrgatronContext _context;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly PartieService _partieService;
        private string _message;
        private readonly IAuthorizationService _authorizationService;

        public RepondreInvitationAutreJoueurModel(OrgatronContext context, UserManager<IdentityUser> userManager, PartieService partieService, IAuthorizationService authorizationService)
        {
            _context = context;
            _userManager = userManager;
            _partieService = partieService;
            _authorizationService = authorizationService;
        }

        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }

        public string IdPartie { get; set; }

        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel
        {
            [BindProperty]
            [Required]
            [DataType(DataType.Password)]
            public string Password { get; set; }

            [BindProperty]
            [Required]
            public bool Comming { get; set; }
        }

        public IActionResult OnGet(int? idInvitation)
        {
            if (idInvitation == null)
            {
                return NotFound();
            }
            IdPartie = _context.Participant.Include(p => p.Partie).Where(p => p.Id == idInvitation).FirstOrDefault().Partie.Id.ToString();
            Input = new InputModel
            {
                Comming = true
            };

            return Page();
        }

        public async System.Threading.Tasks.Task<IActionResult> OnPostAsync(int? idInvitation)
        {

            if (idInvitation == null)
            {
                return NotFound();
            }
            IdPartie = _context.Participant.Include(p => p.Partie).Where(p => p.Id == idInvitation).FirstOrDefault().Partie.Id.ToString();

            if (!ModelState.IsValid)
            {
                return Page();
            }

            Participant invitation = _context.Participant.Include(p => p.User).Include(p => p.Partie.Evenement).Where(p => p.Id == idInvitation).FirstOrDefault();

            bool passwordIsGood = (await _authorizationService.AuthorizeAsync(User, "AdminOnly")).Succeeded || await _userManager.CheckPasswordAsync(invitation.User, Input.Password);

            bool success = false;

            if (passwordIsGood)
            {
                success = _partieService.RepondreInvitation(invitation, _context, EtatInvitation.Rejected, out _message);
            }
            else
            {
                Message = "Le mot de passe est incorrect";
            }

            if (success)
            {
                return RedirectToPage("/Parties/Details", new { invitation.Partie.Id });
            }

            return Page();
        }
    }
}
