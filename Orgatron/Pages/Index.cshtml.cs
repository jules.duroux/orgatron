﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Orgatron.Models;

namespace Orgatron.Pages
{
    public class IndexModel : PageModel
    {
        private OrgatronContext _context;
        private IAuthorizationService _authorizationService;

        public IndexModel(OrgatronContext context, IAuthorizationService authorizationService)
        {
            _context = context;
            _authorizationService = authorizationService;
        }

        [BindProperty]
        public IList<Evenement> Evenements { get; set; }

        public async Task<IActionResult> OnGetAsync()
        {
            Evenements = (from evt in _context.Evenement
                          join evt_usr in _context.EvenementUtilisateur on evt.Id equals evt_usr.Evenement.Id
                          where evt_usr.User.Id == User.FindFirst(ClaimTypes.NameIdentifier).Value
                          select evt).ToList();
            var authorisation = await _authorizationService.AuthorizeAsync(User, "AdminOnly");
            //Si on n'a qu'un seul évènement et qu'on est pas admin on est redirigé dessus
            if (Evenements.Count() == 1 && !authorisation.Succeeded)
            {
                return RedirectToPage("./Evenements/Home", new { Evenements[0].Id });
            }
            //var isAuthorized = await AuthorizationService.AuthorizeAsync(User, Evenement, "EstCreateurEvenement");
            return Page();
        }
    }
}
