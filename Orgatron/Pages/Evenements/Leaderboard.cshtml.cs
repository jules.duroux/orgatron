﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Orgatron.Models;

namespace Orgatron.Pages.Evenements
{
    public class LeaderBoardModel : PageModel
    {
        private readonly Orgatron.Models.OrgatronContext _context;
        private readonly IAuthorizationService _authorizationService;

        public LeaderBoardModel(Orgatron.Models.OrgatronContext context, IAuthorizationService authorizationService)
        {
            _context = context;
            _authorizationService = authorizationService;
        }

        public string IdEvenement { get; set; }

        public List<DisplayModel> Classement { get;set; }

        public class DisplayModel
        {
            public string Joueur { get; set; }
            public int Score { get; set; }
            public double Moyenne { get; set; }
            public int NbParties { get; set; }
            public int NbRefus{ get; set; }
            public int MinutesPause { get; set; }
            public int AjustementModeration { get; set; }
        }

        [BindProperty]
        public InputModel Input { get; set; }
        public class InputModel
        {
            public int? EvenementId { get; set; }
            public string UserName { get; set; }
            public int Points { get; set; }
        }

        public async Task<IActionResult> OnGetAsync(int? idEvenement)
        {
            if (idEvenement == null)
            {
                Evenement evenement = _context.Evenement.Where(e => _context.EvenementUtilisateur.Where(eu => eu.Evenement == e && eu.User.Id == HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value).Any()).OrderByDescending(e => e.StartedAt).FirstOrDefault();
                if (evenement == null)
                {
                    return NotFound();
                }
                idEvenement = evenement.Id;
            }

            IdEvenement = idEvenement.Value.ToString();

            //Participations
            List<Participant> participants = await _context.Participant.Include(p => p.User).Include(p => p.Partie).Where(p => p.Partie.Evenement.Id == idEvenement && p.Etat == EtatInvitation.Accepted && p.Partie.Etat == EtatPartie.Ended).ToListAsync();

            Dictionary<string, DisplayModel> keyValuePairs = new Dictionary<string, DisplayModel>();
            foreach (Participant participant in participants)
            {
                if (!keyValuePairs.ContainsKey(participant.User.UserName))
                {
                    keyValuePairs.Add(participant.User.UserName, new DisplayModel{ Joueur = participant.User.UserName, NbParties = 1, Score = participant.Points, Moyenne = (double)participant.Points });
                }
                else
                {
                    keyValuePairs[participant.User.UserName].Score += participant.Points;
                    keyValuePairs[participant.User.UserName].NbParties ++;
                }
            }

            //Malus refus
            List<Participant> refus = _context.Participant.Include(p => p.User).Include(p => p.Partie).Where(p => p.Partie.Evenement.Id == idEvenement && p.Etat == EtatInvitation.Rejected).ToList();
            foreach (Participant participant in refus)
            {
                if (!keyValuePairs.ContainsKey(participant.User.UserName))
                {
                    keyValuePairs.Add(participant.User.UserName, new DisplayModel { Joueur = participant.User.UserName, NbParties = 0, Score = 0, NbRefus = 1 });
                }
                else
                {
                    keyValuePairs[participant.User.UserName].NbRefus += 1;
                }
            }

            //Malus Pause et ajustement admins
            List<EvenementUtilisateur> pauses = _context.EvenementUtilisateur.Include(eu => eu.User).Where(p => p.Evenement.Id == idEvenement).ToList();
            foreach (EvenementUtilisateur participant in pauses)
            {
                //Ajustement arbitre
                if (!keyValuePairs.ContainsKey(participant.User.UserName))
                {
                    keyValuePairs.Add(participant.User.UserName, new DisplayModel { Joueur = participant.User.UserName, NbParties = 0, Score = 0, Moyenne = 0, NbRefus = 0, MinutesPause = 0, AjustementModeration = participant.ModificateurPoints });
                }
                else
                {
                    int nbRefus = keyValuePairs[participant.User.UserName].AjustementModeration = participant.ModificateurPoints;
                }
                //Malus pause
                if (participant.DateDebutPause != DateTime.MinValue)
                { 
                    int mn = (int)participant.NbSecondesPause;
                    if (participant.EstEnPause)
                    {
                        mn += (int)(new DateTime(Math.Min(DateTime.Now.Ticks,participant.Evenement.EndAt.Ticks)) - participant.DateDebutPause).TotalSeconds;
                    }
                    mn = mn / 60;
                    int nbRefus = keyValuePairs[participant.User.UserName].MinutesPause += mn;
                }
            }

            foreach (DisplayModel classement in keyValuePairs.Values)
            {
                int scoremodifie = classement.Score + classement.AjustementModeration;
                if (classement.NbRefus > 2)
                {
                    scoremodifie -= classement.NbRefus - 2;
                }
                if (classement.MinutesPause > 15)
                {
                    //On ne prend en compte que la partie entière de la division comme ce sont des int
                    scoremodifie -= (classement.MinutesPause)/15;
                }
                classement.Moyenne = (double)scoremodifie / (double)classement.NbParties;
            }
            
            Classement = keyValuePairs.Values.OrderByDescending(v => v.Moyenne).ToList();

            return Page();
        }

        public async Task<IActionResult> OnPostEditScoreAsync()
        {
            int? idEvenement = Input.EvenementId;
            if (idEvenement == null)
            {
                Evenement evenement = _context.Evenement.Where(e => _context.EvenementUtilisateur.Where(eu => eu.Evenement == e && eu.User.Id == HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value).Any()).OrderByDescending(e => e.StartedAt).FirstOrDefault();
                if (evenement == null)
                {
                    return NotFound();
                }
                idEvenement = evenement.Id;
            }
            EvenementUtilisateur evenementUtilisateur = _context.EvenementUtilisateur.Where(eu => eu.User.UserName == Input.UserName && eu.Evenement.Id == idEvenement.Value).FirstOrDefault();

            if (evenementUtilisateur == null)
            {
                return NotFound();
            }

            var isAuthorized = await _authorizationService.AuthorizeAsync(User, "AdminOnly");
            if (!isAuthorized.Succeeded)
            {
                if (User.Identity.IsAuthenticated)
                {
                    return new ForbidResult();
                }
                else
                {
                    return new ChallengeResult();
                }
            }

            evenementUtilisateur.ModificateurPoints = Input.Points;

            _context.SaveChanges();

            return RedirectToPage("/Evenements/Leaderboard", new { idEvenement = Input.EvenementId });

        }
    }
}
