﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Orgatron.Models;

namespace Orgatron.Pages.Evenements
{
    public class HistoriqueModel : PageModel
    {
        private readonly Orgatron.Models.OrgatronContext _context;

        public HistoriqueModel(Orgatron.Models.OrgatronContext context)
        {
            _context = context;
        }

        public string Titre { get; set; }
        public IList<Partie> Partie { get;set; }

        public async Task<IActionResult> OnGetAsync(int? idEvenement, string nomJoueur)
        {
            if (idEvenement == null)
            {
                Evenement evenement = _context.Evenement.Where(e => _context.EvenementUtilisateur.Where(eu => eu.Evenement == e && eu.User.Id == HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value).Any()).OrderByDescending(e => e.StartedAt).FirstOrDefault();
                if (evenement == null)
                {
                    return NotFound();
                }
                idEvenement = evenement.Id;
            }

            if (string.IsNullOrEmpty(nomJoueur))
            {
                Partie = await _context.Partie.Include(p => p.Jeu).Include(p => p.Arbitre).Include(p => p.Participants).ThenInclude(p => p.User).Where(p => p.Evenement.Id == idEvenement).OrderByDescending(p => p.DateCreation).ToListAsync();
                Titre = string.Format("Historique des parties de l'evenement {0}", _context.Evenement.Find(idEvenement.Value).Nom);
            }
            else
            {
                Partie = await _context.Partie.Include(p => p.Jeu).Include(p => p.Arbitre).Include(p => p.Participants).ThenInclude(p => p.User).Where(p => p.Evenement.Id == idEvenement && _context.Participant.Where(j => j.Partie == p && j.User.UserName == nomJoueur).Any()).OrderByDescending(p => p.DateCreation).ToListAsync();
                Titre = string.Format("Historique des parties de {0}", nomJoueur);
            }

            foreach (var partie in Partie)
            {
                partie.Participants = partie.Participants.Where(p => p.Etat != EtatInvitation.Rejected).ToList();
            }
            return Page();
        }
    }
}
