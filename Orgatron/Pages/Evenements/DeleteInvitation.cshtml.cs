﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Orgatron.Models;

namespace Orgatron.Pages.Evenements
{
    public class DeleteInvitationModel : PageModel
    {
        private readonly OrgatronContext _context;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IAuthorizationService _authorizationService;

        public DeleteInvitationModel(OrgatronContext context, UserManager<IdentityUser> userManager, IAuthorizationService authorizationService)
        {
            _context = context;
            _userManager = userManager;
            _authorizationService = authorizationService;
        }

        public async Task<IActionResult> OnGetAsync(string email, int? id)
        {
            if (id == null || email == null)
            {
                return NotFound();
            }

            var user = await _userManager.FindByEmailAsync(email);

            EvenementUtilisateur evenementUtilisateur = _context.EvenementUtilisateur.Where(e => e.Evenement.Id == id && e.User.Id == user.Id).FirstOrDefault();

            bool autorisation = (await _authorizationService.AuthorizeAsync(User, evenementUtilisateur.Evenement, "EstCreateurEvenement")).Succeeded;
            if (!autorisation)
            {
                if (User.Identity.IsAuthenticated)
                {
                    return new ForbidResult();
                }
                else
                {
                    return new ChallengeResult();
                }
            }

            if (evenementUtilisateur != null)
            {
                _context.EvenementUtilisateur.Remove(evenementUtilisateur);
                _context.SaveChanges();

                int nb_events = _context.EvenementUtilisateur.Where(e => e.User.Id == user.Id).Count();
                if (nb_events == 0 && User.FindFirst(ClaimTypes.NameIdentifier).Value != user.Id)
                {
                    await _userManager.DeleteAsync(user);
                }
            }

            return RedirectToPage("./Invitation", new { id });
        }
    }
}