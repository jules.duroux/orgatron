﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Orgatron.Models;

namespace Orgatron.Pages.Evenements
{
    [Authorize(Policy = "AdminOnly")]
    public class IndexModel : PageModel
    {
        private readonly Orgatron.Models.OrgatronContext _context;

        public IndexModel(Orgatron.Models.OrgatronContext context)
        {
            _context = context;
        }

        public IList<Evenement> Evenement { get;set; }

        public async Task OnGetAsync()
        {
            Evenement = await _context.Evenement.ToListAsync();
        }
    }
}
