﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Orgatron.Models;

namespace Orgatron.Pages.Evenements
{
    public class HomeModel : PageModel
    {
        private OrgatronContext _context;
        private IAuthorizationService _authorizationService;

        public HomeModel(OrgatronContext context, IAuthorizationService authorizationService)
        {
            _context = context;
            _authorizationService = authorizationService;
        }

        public IList<string> Messages { get; set; }

        public EvenementUtilisateur EvenementUtilisateur { get; set; }

        public Participant InvitationEnAttente { get; set; }

        public Partie ArbitrageEnAttente { get; set; }

        public Evenement Evenement { get; set; }

        public IdentityUser Utilisateur { get; set; }

        public Partie PartieEnCours { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            EvenementUtilisateur = _context.EvenementUtilisateur.Include(i => i.Evenement).Include(i => i.User)
                .SingleOrDefault(x => x.Evenement.Id == id && x.User.Id == User.FindFirst(ClaimTypes.NameIdentifier).Value);
            Evenement = EvenementUtilisateur.Evenement;
            Utilisateur = EvenementUtilisateur.User;

            var isAuthorized = await _authorizationService.AuthorizeAsync(User, EvenementUtilisateur, "UserInEvenement");
            if (!isAuthorized.Succeeded)
            {
                if (User.Identity.IsAuthenticated)
                {
                    return new ForbidResult();
                }
                else
                {
                    return new ChallengeResult();
                }
            }

            InvitationEnAttente = _context.Participant.Include(p => p.Partie).Include(p => p.Partie.Jeu).Where(p => p.User.Id == User.FindFirst(ClaimTypes.NameIdentifier).Value && p.Etat != EtatInvitation.Rejected && p.Partie.Etat == EtatPartie.Created).FirstOrDefault();

            ArbitrageEnAttente = _context.Partie.Include(p => p.Jeu).Where(p => p.Arbitre.Id == User.FindFirst(ClaimTypes.NameIdentifier).Value && p.EtatArbitre != EtatInvitation.Rejected && p.Etat == EtatPartie.Created).FirstOrDefault();

            Participant participationPartieEnCours = _context.Participant.Include(p => p.Partie).Include(p => p.Partie.Jeu).Where(p => p.User.Id == User.FindFirst(ClaimTypes.NameIdentifier).Value && p.Etat == EtatInvitation.Accepted && (p.Partie.Etat == EtatPartie.Created || p.Partie.Etat == EtatPartie.Started)).OrderByDescending(p => p.Id).FirstOrDefault();

            if (participationPartieEnCours != null)
            {
                PartieEnCours = participationPartieEnCours.Partie;
            }
            else
            {
                PartieEnCours = _context.Partie.Include(p => p.Jeu).Where(p => p.Arbitre.Id == User.FindFirst(ClaimTypes.NameIdentifier).Value && p.EtatArbitre == EtatInvitation.Accepted && (p.Etat == EtatPartie.Created || p.Etat == EtatPartie.Started)).OrderByDescending(p => p.Id).FirstOrDefault();
            }

            return Page();
        }
    }
}