﻿using System;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Orgatron.Models;
using Orgatron.Services;

namespace Orgatron.Pages.Evenements
{
    public class ReinitiatisationCompteModel : PageModel
    {
        private readonly OrgatronContext _context;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IAuthorizationService _authorizationService;

        public ReinitiatisationCompteModel(OrgatronContext context, UserManager<IdentityUser> userManager, IAuthorizationService authorizationService)
        {
            _context = context;
            _userManager = userManager;
            _authorizationService = authorizationService;
        }


        public async System.Threading.Tasks.Task<IActionResult> OnGetAsync(int? IdEvenement, string playerName)
        {

            IdentityUser utilisateur = await _userManager.FindByNameAsync(playerName);
            if (IdEvenement == null || utilisateur == null)
            {
                return NotFound();
            }
            EvenementUtilisateur evenementUtilisateur = _context.EvenementUtilisateur.Include(e => e.Evenement).Include(e => e.User).Include(e => e.Evenement.Createur).Where(e => e.Evenement.Id == IdEvenement.Value && e.User.Id == utilisateur.Id).FirstOrDefault();

            bool autorisation = (await _authorizationService.AuthorizeAsync(User, "AdminOnly")).Succeeded;
            if (!autorisation)
            {
                if (User.Identity.IsAuthenticated)
                {
                    return new ForbidResult();
                }
                else
                {
                    return new ChallengeResult();
                }
            }

            IdentityUser user = _context.Users.Where(u => u.Id == evenementUtilisateur.User.Id).FirstOrDefault();
            user.EmailConfirmed = false;
            user.PasswordHash = null;
            user.LockoutEnd = null;
            user.AccessFailedCount = 0;

        _context.SaveChanges();
            
            return RedirectToPage("/Evenements/Invitation", new { id = IdEvenement });
        }
    }
}
