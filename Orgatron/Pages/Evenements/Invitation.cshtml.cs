﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Orgatron.Models;

namespace Orgatron.Pages.Evenements
{
    public class InvitationModel : PageModel
    {

        private readonly IEmailSender _emailSender;
        private readonly IAuthorizationService _authorizationService;
        private readonly OrgatronContext _context;
        private readonly UserManager<IdentityUser> _userManager;

        public InvitationModel(
            OrgatronContext context,
            IAuthorizationService authorizationService,
            IEmailSender emailSender,
            UserManager<IdentityUser> userManager)
        {
                _emailSender = emailSender;
            _authorizationService = authorizationService;
            _context = context;
            _userManager = userManager;

        }
        
        public Evenement Evenement { get; set; }

        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel
        {
            [Required]
            [StringLength(100, ErrorMessage = "Le {0} doit y avoir au minimum {2} caractères et au maximum {1} caractères.", MinimumLength = 3)]
            public string Name { get; set; }

            [Required]
            [EmailAddress]
            public string Email { get; set; }
        }
        
        private async Task SendInscriptionMailAsync(IdentityUser user)
        {
            var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            var callbackUrl = Url.Page(
                    "/Inscription",
                    pageHandler: null,
                    values: new { code, user.Email },
                    protocol: Request.Scheme);

            await _emailSender.SendEmailAsync(
                user.Email,
                "Inscription",
                $"Bonjour,<br/>" +
                $"Ceci est votre invitation à l'application Orgatron qui va gérer l'organisation des parties.<br/>" +
                $"Pour vous connectez, commencez par vous connecter au Wifi \"Durzooland\" à Charols et <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>cliquez sur ce lien </a>.<br/>" +
                $"<br/>" +
                $"A tout de suite :)<br/>" +
                $"Jules<br/>");
        }

        public async Task<IActionResult> OnGetAsync(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }

            Evenement = _context.Evenement.Include(i => i.Createur).Include(i => i.EvenementsUtilisateurs).ThenInclude(eu => eu.User).SingleOrDefault(x => x.Id == id);

            // l'utilisateur est-il le créateur de l'évènement ?
            var isAuthorized = await _authorizationService.AuthorizeAsync(User, "AdminOnly");
            if (!isAuthorized.Succeeded)
            {
                if (User.Identity.IsAuthenticated)
                {
                    return new ForbidResult();
                }
                else
                {
                    return new ChallengeResult();
                }
            }

            return Page();
        }

        public async Task<IActionResult> OnPostSendMailAsync(string email, int? id)
        {

            Evenement = _context.Evenement.Include(i => i.Createur).Include(i => i.EvenementsUtilisateurs).ThenInclude(eu => eu.User).SingleOrDefault(x => x.Id == id);
            var isAuthorized = await _authorizationService.AuthorizeAsync(User, "AdminOnly");
            if (!isAuthorized.Succeeded)
            {
                if (User.Identity.IsAuthenticated)
                {
                    return new ForbidResult();
                }
                else
                {
                    return new ChallengeResult();
                }
            }

            if (email == null)
            {
                ModelState.AddModelError(string.Empty, "Il faut choisir un utilisateur avec une adresse mail");
                return Page();
            }

            var user = await _userManager.FindByEmailAsync(Input.Email);
            await SendInscriptionMailAsync(user);

            return RedirectToPage("./Invitation", new { id });
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            Evenement = _context.Evenement.Include(i => i.Createur).Include(i => i.EvenementsUtilisateurs).ThenInclude(eu => eu.User).SingleOrDefault(x => x.Id == id);

            // l'utilisateur est-il le créateur de l'évènement ?
            var isAuthorized = await _authorizationService.AuthorizeAsync(User, "AdminOnly");
            if (!isAuthorized.Succeeded)
            {
                if (User.Identity.IsAuthenticated)
                {
                    return new ForbidResult();
                }
                else
                {
                    return new ChallengeResult();
                }
            }

            var user = await _userManager.FindByEmailAsync(Input.Email);
            if (user == null)
            {
                var userWithSameName = await _userManager.FindByNameAsync(Input.Name);
                if (userWithSameName != null)
                {
                    ModelState.AddModelError(string.Empty, "Un autre utilisateur utilise déjà ce nom");
                    return Page();
                }
                // On crée le user
                user = new IdentityUser(Input.Name)
                {
                    Email = Input.Email
                };
                await _userManager.CreateAsync(user);
                await _userManager.UpdateNormalizedEmailAsync(user);

                await SendInscriptionMailAsync(user);
            }

            EvenementUtilisateur evenementUtilisateur = Evenement.EvenementsUtilisateurs.Where(e => e.Evenement.Id == id && e.User.Id == user.Id).FirstOrDefault();

            if (evenementUtilisateur == null)
            {
                evenementUtilisateur = new EvenementUtilisateur
                {
                    User = user,
                    Evenement = Evenement
                };
                _context.EvenementUtilisateur.Add(evenementUtilisateur);
                _context.SaveChanges();
            }

            return RedirectToPage("./Invitation", new { id });
        }
    }
}