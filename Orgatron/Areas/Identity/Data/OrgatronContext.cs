﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Orgatron.Models;

namespace Orgatron.Models
{
    public class OrgatronContext : IdentityDbContext<IdentityUser>
    {
        public OrgatronContext(DbContextOptions<OrgatronContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }

        public DbSet<Evenement> Evenement { get; set; }

        public DbSet<EvenementUtilisateur> EvenementUtilisateur { get; set; }

        public DbSet<Jeu> Jeu { get; set; }

        public DbSet<Partie> Partie { get; set; }

        public DbSet<Participant> Participant { get; set; }
    }
}
