﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Orgatron.Models;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Security.Claims;

namespace Orgatron.Seeds
{
    public static class UsersData
    {
        public static async Task SeedAdmins(IServiceProvider serviceProvider, List<Tuple<string, string>> AdminList)
        {
            using (var serviceScope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var dbContext = serviceScope.ServiceProvider.GetService<OrgatronContext>();
                var userManager = serviceScope.ServiceProvider.GetRequiredService<UserManager<IdentityUser>>();
                
                foreach (var admin in AdminList)
                {
                    var user = await userManager.FindByEmailAsync(admin.Item1);
                    if (user == null)
                    {
                        user = new IdentityUser(admin.Item1)
                        {
                            Email = admin.Item1
                        };
                        var result = await userManager.CreateAsync(user);
                        await userManager.UpdateNormalizedEmailAsync(user);
                        result = await userManager.SetUserNameAsync(user, admin.Item1);
                        result = await userManager.AddPasswordAsync(user, admin.Item2);
                        string token = await userManager.GenerateEmailConfirmationTokenAsync(user);
                        result = await userManager.ConfirmEmailAsync(user, token);
                        result = await userManager.AddClaimAsync(user, new Claim("Admin","Admin"));

                    }
                }
    
            }
        }
    }
}
