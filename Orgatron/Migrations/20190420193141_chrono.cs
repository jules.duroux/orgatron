﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Orgatron.Migrations
{
    public partial class chrono : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DateDebutChrono",
                table: "Partie",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "DateDebutPause",
                table: "Partie",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "EtatChrono",
                table: "Partie",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<double>(
                name: "NbSecondesPauseChrono",
                table: "Partie",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<string>(
                name: "NotesArbitre",
                table: "Partie",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateDebutChrono",
                table: "Partie");

            migrationBuilder.DropColumn(
                name: "DateDebutPause",
                table: "Partie");

            migrationBuilder.DropColumn(
                name: "EtatChrono",
                table: "Partie");

            migrationBuilder.DropColumn(
                name: "NbSecondesPauseChrono",
                table: "Partie");

            migrationBuilder.DropColumn(
                name: "NotesArbitre",
                table: "Partie");
        }
    }
}
