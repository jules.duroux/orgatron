﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Orgatron.Migrations
{
    public partial class cancelupdateevenementUtilisateur : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserName",
                table: "EvenementUtilisateur");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "UserName",
                table: "EvenementUtilisateur",
                maxLength: 100,
                nullable: true);
        }
    }
}
