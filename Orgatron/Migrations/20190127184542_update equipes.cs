﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Orgatron.Migrations
{
    public partial class updateequipes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "EvenementID",
                table: "Equipe",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Equipe_EvenementID",
                table: "Equipe",
                column: "EvenementID");

            migrationBuilder.AddForeignKey(
                name: "FK_Equipe_Evenement_EvenementID",
                table: "Equipe",
                column: "EvenementID",
                principalTable: "Evenement",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Equipe_Evenement_EvenementID",
                table: "Equipe");

            migrationBuilder.DropIndex(
                name: "IX_Equipe_EvenementID",
                table: "Equipe");

            migrationBuilder.DropColumn(
                name: "EvenementID",
                table: "Equipe");
        }
    }
}
