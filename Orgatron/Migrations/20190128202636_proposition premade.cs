﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Orgatron.Migrations
{
    public partial class propositionpremade : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EvenementUtilisateur_Equipe_EquipeID",
                table: "EvenementUtilisateur");

            migrationBuilder.RenameColumn(
                name: "EquipeID",
                table: "EvenementUtilisateur",
                newName: "EquipeId");

            migrationBuilder.RenameIndex(
                name: "IX_EvenementUtilisateur_EquipeID",
                table: "EvenementUtilisateur",
                newName: "IX_EvenementUtilisateur_EquipeId");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "Equipe",
                newName: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_EvenementUtilisateur_Equipe_EquipeId",
                table: "EvenementUtilisateur",
                column: "EquipeId",
                principalTable: "Equipe",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EvenementUtilisateur_Equipe_EquipeId",
                table: "EvenementUtilisateur");

            migrationBuilder.RenameColumn(
                name: "EquipeId",
                table: "EvenementUtilisateur",
                newName: "EquipeID");

            migrationBuilder.RenameIndex(
                name: "IX_EvenementUtilisateur_EquipeId",
                table: "EvenementUtilisateur",
                newName: "IX_EvenementUtilisateur_EquipeID");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Equipe",
                newName: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_EvenementUtilisateur_Equipe_EquipeID",
                table: "EvenementUtilisateur",
                column: "EquipeID",
                principalTable: "Equipe",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
