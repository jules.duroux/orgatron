﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Orgatron.Migrations
{
    public partial class EvenementUtilisateur2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EvenementUtilisateur",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EvenementID = table.Column<int>(nullable: true),
                    EquipeID = table.Column<int>(nullable: true),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EvenementUtilisateur", x => x.ID);
                    table.ForeignKey(
                        name: "FK_EvenementUtilisateur_Equipe_EquipeID",
                        column: x => x.EquipeID,
                        principalTable: "Equipe",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EvenementUtilisateur_Evenement_EvenementID",
                        column: x => x.EvenementID,
                        principalTable: "Evenement",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EvenementUtilisateur_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EvenementUtilisateur_EquipeID",
                table: "EvenementUtilisateur",
                column: "EquipeID");

            migrationBuilder.CreateIndex(
                name: "IX_EvenementUtilisateur_EvenementID",
                table: "EvenementUtilisateur",
                column: "EvenementID");

            migrationBuilder.CreateIndex(
                name: "IX_EvenementUtilisateur_UserId",
                table: "EvenementUtilisateur",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EvenementUtilisateur");
        }
    }
}
