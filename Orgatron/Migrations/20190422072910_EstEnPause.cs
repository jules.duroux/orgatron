﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Orgatron.Migrations
{
    public partial class EstEnPause : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "EstActif",
                table: "EvenementUtilisateur",
                newName: "EstEnPause");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "EstEnPause",
                table: "EvenementUtilisateur",
                newName: "EstActif");
        }
    }
}
