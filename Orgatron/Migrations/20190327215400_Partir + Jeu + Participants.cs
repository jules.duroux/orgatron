﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Orgatron.Migrations
{
    public partial class PartirJeuParticipants : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "EstActif",
                table: "Jeu",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "NbEquipesMax",
                table: "Jeu",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "NbEquipesMin",
                table: "Jeu",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "NbParticipantsParEquipesMax",
                table: "Jeu",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "NbParticipantsParEquipesMin",
                table: "Jeu",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "nbPersonnesEnAttente",
                table: "Evenement",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Partie",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    JeuId = table.Column<int>(nullable: true),
                    EvenementId = table.Column<int>(nullable: true),
                    ArbitreId = table.Column<string>(nullable: true),
                    Etat = table.Column<int>(nullable: false),
                    DateCreation = table.Column<DateTime>(nullable: false),
                    DateDebut = table.Column<DateTime>(nullable: false),
                    DateFin = table.Column<DateTime>(nullable: false),
                    EtatArbitre = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Partie", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Partie_AspNetUsers_ArbitreId",
                        column: x => x.ArbitreId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Partie_Evenement_EvenementId",
                        column: x => x.EvenementId,
                        principalTable: "Evenement",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Partie_Jeu_JeuId",
                        column: x => x.JeuId,
                        principalTable: "Jeu",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Participant",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PartieId = table.Column<int>(nullable: true),
                    UserId = table.Column<string>(nullable: true),
                    Equipe = table.Column<string>(nullable: true),
                    Score = table.Column<int>(nullable: false),
                    Points = table.Column<int>(nullable: false),
                    Etat = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Participant", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Participant_Partie_PartieId",
                        column: x => x.PartieId,
                        principalTable: "Partie",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Participant_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Participant_PartieId",
                table: "Participant",
                column: "PartieId");

            migrationBuilder.CreateIndex(
                name: "IX_Participant_UserId",
                table: "Participant",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Partie_ArbitreId",
                table: "Partie",
                column: "ArbitreId");

            migrationBuilder.CreateIndex(
                name: "IX_Partie_EvenementId",
                table: "Partie",
                column: "EvenementId");

            migrationBuilder.CreateIndex(
                name: "IX_Partie_JeuId",
                table: "Partie",
                column: "JeuId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Participant");

            migrationBuilder.DropTable(
                name: "Partie");

            migrationBuilder.DropColumn(
                name: "EstActif",
                table: "Jeu");

            migrationBuilder.DropColumn(
                name: "NbEquipesMax",
                table: "Jeu");

            migrationBuilder.DropColumn(
                name: "NbEquipesMin",
                table: "Jeu");

            migrationBuilder.DropColumn(
                name: "NbParticipantsParEquipesMax",
                table: "Jeu");

            migrationBuilder.DropColumn(
                name: "NbParticipantsParEquipesMin",
                table: "Jeu");

            migrationBuilder.DropColumn(
                name: "nbPersonnesEnAttente",
                table: "Evenement");
        }
    }
}
