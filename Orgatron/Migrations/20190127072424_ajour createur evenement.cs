﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Orgatron.Migrations
{
    public partial class ajourcreateurevenement : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CreateurId",
                table: "Evenement",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Evenement_CreateurId",
                table: "Evenement",
                column: "CreateurId");

            migrationBuilder.AddForeignKey(
                name: "FK_Evenement_AspNetUsers_CreateurId",
                table: "Evenement",
                column: "CreateurId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Evenement_AspNetUsers_CreateurId",
                table: "Evenement");

            migrationBuilder.DropIndex(
                name: "IX_Evenement_CreateurId",
                table: "Evenement");

            migrationBuilder.DropColumn(
                name: "CreateurId",
                table: "Evenement");
        }
    }
}
