﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Orgatron.Migrations
{
    public partial class calcultempspause : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EstHandicap",
                table: "EvenementUtilisateur");

            migrationBuilder.AddColumn<DateTime>(
                name: "DateDebutPause",
                table: "EvenementUtilisateur",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<double>(
                name: "NbSecondesPause",
                table: "EvenementUtilisateur",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateDebutPause",
                table: "EvenementUtilisateur");

            migrationBuilder.DropColumn(
                name: "NbSecondesPause",
                table: "EvenementUtilisateur");

            migrationBuilder.AddColumn<bool>(
                name: "EstHandicap",
                table: "EvenementUtilisateur",
                nullable: false,
                defaultValue: false);
        }
    }
}
