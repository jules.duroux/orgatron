﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Orgatron.Migrations
{
    public partial class Classement : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Score",
                table: "Participant",
                newName: "Classement");

            migrationBuilder.AddColumn<string>(
                name: "Notes",
                table: "Participant",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Notes",
                table: "Participant");

            migrationBuilder.RenameColumn(
                name: "Classement",
                table: "Participant",
                newName: "Score");
        }
    }
}
