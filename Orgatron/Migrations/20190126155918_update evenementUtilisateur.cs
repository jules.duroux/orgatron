﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Orgatron.Migrations
{
    public partial class updateevenementUtilisateur : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "UserName",
                table: "EvenementUtilisateur",
                maxLength: 100,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserName",
                table: "EvenementUtilisateur");
        }
    }
}
