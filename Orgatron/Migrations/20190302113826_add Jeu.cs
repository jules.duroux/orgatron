﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Orgatron.Migrations
{
    public partial class addJeu : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EvenementUtilisateur_Equipe_EquipeId",
                table: "EvenementUtilisateur");

            migrationBuilder.DropTable(
                name: "Equipe");

            migrationBuilder.DropTable(
                name: "PropositionPremade");

            migrationBuilder.DropIndex(
                name: "IX_EvenementUtilisateur_EquipeId",
                table: "EvenementUtilisateur");

            migrationBuilder.DropColumn(
                name: "EquipeId",
                table: "EvenementUtilisateur");

            migrationBuilder.DropColumn(
                name: "nbEquipes",
                table: "Evenement");

            migrationBuilder.DropColumn(
                name: "nbJoueurs",
                table: "Evenement");

            migrationBuilder.DropColumn(
                name: "tailleRegroupements",
                table: "Evenement");

            migrationBuilder.AddColumn<DateTime>(
                name: "EndAt",
                table: "Evenement",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "StartedAt",
                table: "Evenement",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EndAt",
                table: "Evenement");

            migrationBuilder.DropColumn(
                name: "StartedAt",
                table: "Evenement");

            migrationBuilder.AddColumn<int>(
                name: "EquipeId",
                table: "EvenementUtilisateur",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "nbEquipes",
                table: "Evenement",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<short>(
                name: "nbJoueurs",
                table: "Evenement",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<short>(
                name: "tailleRegroupements",
                table: "Evenement",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.CreateTable(
                name: "Equipe",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EvenementId = table.Column<int>(nullable: true),
                    Nom = table.Column<string>(nullable: true),
                    Points = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Equipe", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Equipe_Evenement_EvenementId",
                        column: x => x.EvenementId,
                        principalTable: "Evenement",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PropositionPremade",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EstValide = table.Column<bool>(nullable: false),
                    EvenementId = table.Column<int>(nullable: true),
                    HebergeurId = table.Column<string>(nullable: true),
                    InviteId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PropositionPremade", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PropositionPremade_Evenement_EvenementId",
                        column: x => x.EvenementId,
                        principalTable: "Evenement",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PropositionPremade_AspNetUsers_HebergeurId",
                        column: x => x.HebergeurId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PropositionPremade_AspNetUsers_InviteId",
                        column: x => x.InviteId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EvenementUtilisateur_EquipeId",
                table: "EvenementUtilisateur",
                column: "EquipeId");

            migrationBuilder.CreateIndex(
                name: "IX_Equipe_EvenementId",
                table: "Equipe",
                column: "EvenementId");

            migrationBuilder.CreateIndex(
                name: "IX_PropositionPremade_EvenementId",
                table: "PropositionPremade",
                column: "EvenementId");

            migrationBuilder.CreateIndex(
                name: "IX_PropositionPremade_HebergeurId",
                table: "PropositionPremade",
                column: "HebergeurId");

            migrationBuilder.CreateIndex(
                name: "IX_PropositionPremade_InviteId",
                table: "PropositionPremade",
                column: "InviteId");

            migrationBuilder.AddForeignKey(
                name: "FK_EvenementUtilisateur_Equipe_EquipeId",
                table: "EvenementUtilisateur",
                column: "EquipeId",
                principalTable: "Equipe",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
