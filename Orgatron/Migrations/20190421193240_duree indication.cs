﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Orgatron.Migrations
{
    public partial class dureeindication : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DureeIndicative",
                table: "Jeu",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DureeIndicative",
                table: "Jeu");
        }
    }
}
