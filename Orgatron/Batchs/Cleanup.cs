﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Orgatron.Models;
using Orgatron.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Orgatron.Batchs
{
    public class Cleanup : BackgroundService
    {
        private readonly PartieService _partieService;
        private readonly IConfiguration _configuration;

        public Cleanup(PartieService partieService, IConfiguration configuration)
        {
            _partieService = partieService;
            _configuration = configuration;
        }
        protected async override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                DbContextOptionsBuilder<OrgatronContext> optionsBuilder = new DbContextOptionsBuilder<OrgatronContext>();
                optionsBuilder.UseSqlServer(_configuration.GetConnectionString("OrgatronContextConnection"));
                using (OrgatronContext context = new OrgatronContext(optionsBuilder.Options))
                {
                    List<Evenement> evenementsDemarres = context.Evenement.Where(e => e.StartedAt != DateTime.MinValue && e.EndAt > DateTime.Now).ToList();

                    foreach (Evenement evenement in evenementsDemarres)
                    {
                        _partieService.LancerParties(evenement, context);
                    }
                }
                   
                 await Task.Delay(60000, stoppingToken);
            }
        }
    }
}